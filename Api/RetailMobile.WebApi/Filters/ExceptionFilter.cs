﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RetailMobile.WebApi.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        // simple global exception handler, convert the exeption to bad request
        // in real, will log exception, send alert and might have standard 
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            
            context.ExceptionHandled = true;
            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.InternalServerError;
            response.ContentType = "application/json";
            var result = new BadRequestObjectResult(new { error = exception.Message });
            context.Result = result;
        }
    }
}
