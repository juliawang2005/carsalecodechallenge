﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailMobile.Core.Interfaces;
using RetailMobile.Core.Models;

namespace RetailMobile.WebApi.Controllers
{

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IRetailMobile _client;
        public CarsController(IRetailMobile client)
        {
            this._client = client ?? throw new ArgumentNullException(nameof(client));
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(IEnumerable<CarSummary>), StatusCodes.Status200OK)]
        public async Task<IActionResult> List()
        {
            return Ok(await _client.GetCars());

        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CarDetail), StatusCodes.Status200OK)]
        public async Task<ActionResult> GetById(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest("Please input a valid id");
            }

            // retail-interview-api.preprod.retail.csnglobal.net will throw 502 exception when id is not valid ??
            // not sure business logic here, but we did not get a empty result
            // might be id is not match rule or special requirment in id etc, Exception handler will handle it
            // do not need handler it
            var car = await _client.GetCarDetails(id);
            return Ok(car);
        }

    }
}
