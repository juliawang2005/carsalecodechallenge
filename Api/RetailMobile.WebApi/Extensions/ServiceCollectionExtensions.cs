﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RetailMobile.Core.Clients;
using RetailMobile.Core.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Text;

namespace RetailMobile.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var carsalesServiceOptions = new CarSalesServiceOptions();
            configuration.GetSection("CarSalesServiceOptions").Bind(carsalesServiceOptions);

            var baseAuthHandler = new HttpClientHandler()
            {
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(configuration.GetValue<string>("CarSalesWebApiUsername"),
                    configuration.GetValue<string>("CarSalesWebApiPassword")),
            };

            services.AddHttpClient("CarSalesClient", (client) =>
            {
                client.BaseAddress = new Uri(configuration.GetValue<string>("CarSalesBaseUri"));
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                var byteArray = Encoding.ASCII.GetBytes($"{configuration.GetValue<string>("CarSalesWebApiUsername")}:{configuration.GetValue<string>("CarSalesWebApiPassword")}");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            })
            .AddTypedClient<IRetailMobile>(c => new CarSalesClient(c, carsalesServiceOptions));

            return services;
        }
    }
}
