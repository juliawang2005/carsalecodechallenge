﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using RetailMobile.Core.Interfaces;
using RetailMobile.Core.Models;
using RetailMobile.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RetailMobile.Tests
{
    public class RetailMobileCarsControllerTests
    {
        private readonly CarsController _controller;
        private readonly Mock<IRetailMobile> _mockClient;
        
        public RetailMobileCarsControllerTests()
        {
            this._mockClient = new Mock<IRetailMobile>();
            this._controller = new CarsController(this._mockClient.Object);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public async Task GetById_ShouldReturnBadRequestWhenIdEmpty(string id)
        {
            var result = await _controller.GetById(id);
            Assert.NotNull(result);
            Assert.IsAssignableFrom<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task GetById_ShouldReturnOkResult()
        {
            // Arrange
            var carId = "T00001";
            this._mockClient.Setup(x => x.GetCarDetails(It.IsAny<string>())).ReturnsAsync(new Core.Models.CarDetail
            {
                Id = carId
            });


            // Act
            var result = await this._controller.GetById("test");

            // Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);

            var okResult = result as OkObjectResult;
            Assert.IsType<CarDetail>(okResult.Value);

            var expectedCar = okResult.Value as CarDetail;
            Assert.Equal(expectedCar.Id, carId);
        }

    }
}
