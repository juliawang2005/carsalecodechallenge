﻿namespace RetailMobile.Core.Models
{
    public class Car
    {
        public string Id { get; set; }
        public string ListingType { get; set; }
        public int Odometer { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public int Price { get; set; }
        public string SellerType { get; set; }
    }
}
