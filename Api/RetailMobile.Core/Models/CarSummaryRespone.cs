﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetailMobile.Core.Models
{
    public class CarSummaryRespone
    {
        public CarSummaryRespone()
        {
            Items = new List<CarSummary>();
        }
        public List<CarSummary> Items { get; set; }
    }
}
