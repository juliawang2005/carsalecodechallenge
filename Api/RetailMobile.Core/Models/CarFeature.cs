﻿namespace RetailMobile.Core.Models
{
    public class CarFeature
    {
        public string Name { get; set; }
        public string Group { get; set; }
    }
}
