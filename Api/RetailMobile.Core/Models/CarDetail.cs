﻿using System.Collections.Generic;

namespace RetailMobile.Core.Models
{
    public class CarDetail : Car
    {
        public CarDetail()
        {
            Photos = new List<string>();
            Features = new List<CarFeature>();
        }

        public IEnumerable<string> Photos { get; set; }
        public IEnumerable<CarFeature> Features { get; set; }
    }
}
