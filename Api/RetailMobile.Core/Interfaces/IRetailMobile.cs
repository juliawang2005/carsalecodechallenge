﻿using RetailMobile.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RetailMobile.Core.Interfaces
{
    public interface IRetailMobile
    {
        /// <summary>Get list of cars</summary>
        /// <returns>
        ///  car summaries
        /// </returns>
        Task<IEnumerable<CarSummary>> GetCars();


        /// <summary>Gets the car details.</summary>
        /// <param name="id">The car identifier.</param>
        /// <returns>
        ///   car details
        /// </returns>
        Task<CarDetail> GetCarDetails(string id);
    }
}
