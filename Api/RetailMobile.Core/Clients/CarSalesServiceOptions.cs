﻿namespace RetailMobile.Core.Clients
{
    public class CarSalesServiceOptions
    {
        public string CarListUri { get; set; }
        public string CarDetailsUri { get; set; }
    }
}
