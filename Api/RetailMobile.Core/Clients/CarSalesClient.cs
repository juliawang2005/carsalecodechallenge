﻿using Newtonsoft.Json;
using RetailMobile.Core.Interfaces;
using RetailMobile.Core.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RetailMobile.Core.Clients
{
    public class CarSalesClient : IRetailMobile
    {
        private readonly HttpClient _httpClient;
        private readonly CarSalesServiceOptions _options;
        public CarSalesClient(HttpClient httpClient, CarSalesServiceOptions options)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));


            _options = options ?? throw new ArgumentNullException(nameof(options));

            if (string.IsNullOrWhiteSpace(options.CarDetailsUri))
            {
                throw new ArgumentNullException(nameof(options.CarDetailsUri));
            }

            if (string.IsNullOrWhiteSpace(options.CarListUri))
            {
                throw new ArgumentNullException(nameof(options.CarListUri));
            }
        }

        public async Task<CarDetail> GetCarDetails(string id)
        {
            var uri = $"{this._options.CarDetailsUri}/{id}";
            var response = await _httpClient.GetStringAsync(uri);
            return JsonConvert.DeserializeObject<CarDetail>(response);
        }

        public async Task<IEnumerable<CarSummary>> GetCars()
        {
            var uri = this._options.CarListUri;
            var response = await _httpClient.GetStringAsync(uri);
            var results = JsonConvert.DeserializeObject<CarSummaryRespone>(response);
             return results?.Items ?? new List<CarSummary>();
        }
    }
}
