# Carsales Coding Challenge

Retail mobile project for car sales coding challenge.

## Pre Requirements

### Local Development

- Api: .NET Core SDK 3.1, Visual Studio 2019
- Web: Angular 10.0.

### Run in local development environment

- You could run Api with Visual Studio 2019.
- You could start Web with command in `Web` folder:
  - `npm install` (only need this for first time)
  - `ng serve`
- Please leave the Api local port as 5000, otherwise please update the environment configuration.

## System Overview

This is Retail Mobile website which provide:

- A listing page which display all available stock.
- A detail page to view more information about stock.
- Front end use Angular 10 and ngx-bootstrap.

**Assumptions:**

- No authentication and authorization.
- Data validation is basic. Only check id is empty or white spacel
- No pagination, return all results from retail-interview-api.preprod.retail.csnglobal.net, in real world might need pagination or limit to xxx records etc.
- Error handling is very basic, only use exception handler and convert exception to a Json.
- No logger, need future information.
- Added ApiVersioning.
- Added Swagger.

## Test

Only write the basic unit test which include

- Api: uses xUnit, Moq. You could run it with Visual Studio.
- Web: did not write custom unit test for Web project. The business logic is very simple just get and display. Could write test when needed.

## Folder Description

- Web: Frontend source code
- Api: WebApi source code
