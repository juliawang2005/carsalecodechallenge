export interface CarFeature {
  name: string;
  group: string;
}
