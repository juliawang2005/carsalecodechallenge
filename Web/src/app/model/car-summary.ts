import { ListTypeEnum } from '../enum/list-type-enum';
import { SellerTypeEnum } from '../enum/seller-type-enum';
import { StateEnum } from '../enum/state-enum';

export interface CarSummary {
  photo: string;
  id: number;
  listingType: ListTypeEnum;
  odometer: number;
  title: string;
  state: StateEnum;
  price: number;
  sellerType: SellerTypeEnum;
}
