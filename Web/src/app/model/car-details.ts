import { CarFeature } from './car-feature';
import { CarSummary } from './car-summary';

export interface CarDetails extends CarSummary {
  photos: string[];
  features: CarFeature[];
}
