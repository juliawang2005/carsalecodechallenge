import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarCardContainerComponent } from './car-card-container.component';

describe('CarCardContainerComponent', () => {
  let component: CarCardContainerComponent;
  let fixture: ComponentFixture<CarCardContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarCardContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarCardContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
