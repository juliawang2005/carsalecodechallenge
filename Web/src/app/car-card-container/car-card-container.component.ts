import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CarSummary } from '../model/car-summary';

@Component({
  selector: 'app-car-card-container',
  templateUrl: './car-card-container.component.html',
  styleUrls: ['./car-card-container.component.scss'],
})
export class CarCardContainerComponent implements OnInit {
  @Input() car: CarSummary;
  @Output() clickItem = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  onClick(): void {
    this.clickItem.emit(this.car.id);
  }
}
