import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarSummary } from '../model/car-summary';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss'],
})
export class CarListComponent implements OnInit {
  carSummaries: CarSummary[];
  routToCarDetail = '';
  constructor(private router: Router, private carService: CarService) {}

  ngOnInit(): void {
    this.carService.getCarSummaryList().subscribe((result) => {
      this.carSummaries = result;
      this.routToCarDetail = `/cars/`;
    });
  }

  onCarClicked(id: number): void {
    this.router.navigate([`/cars/${id}`]);
  }
}

