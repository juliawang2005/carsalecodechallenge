import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CarSummary } from "../model/car-summary";
import { environment } from "src/environments/environment";
import { Observable, from, of } from "rxjs";
import { map, tap, filter, take, catchError } from "rxjs/operators";
import { Toast, ToastrService } from "ngx-toastr";
import { CarDetails } from "../model/car-details";

@Injectable({
  providedIn: "root",
})
export class CarService {
  httpOptions = {
    headers: new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
    }),
  };
  constructor(private httpClient: HttpClient, private toastr: ToastrService) {}

  getCarSummaryList(): Observable<CarSummary[]> {
    const url = `${environment.api_base}/cars`;
    return this.httpClient.get<CarSummary[]>(url, this.httpOptions).pipe(
      map((car) => {
        return car.map((c) => this.getCarSummaryFromJSON(c));
      }),
      catchError((_) => {
        this.toastr.error("Unable to get car summary list.");
        return of(null);
      })
    );
  }

  getCarDetails(id: number): Observable<CarDetails> {
    const url = `${environment.api_base}/cars/${id}`;
    return this.httpClient.get<CarDetails>(url, this.httpOptions).pipe(
      map((car) => this.getCarDetailsFromJSON(car)),
      catchError((_) => {
        this.toastr.error(`Unable to get car (id=${id}) deatails`);
        return of(null);
      })
    );
  }

  private getCarSummaryFromJSON(json: any): CarSummary {
    const car: CarSummary = {
      photo: json.photo,
      id: json.id,
      listingType: json.listingType,
      odometer: json.odometer,
      title: json.title,
      state: json.state,
      price: json.price,
      sellerType: json.sellerType,
    };
    return car;
  }

  private getCarDetailsFromJSON(json: any): CarDetails {
    const car: CarDetails = {
      photo: json.photo,
      id: json.id,
      listingType: json.listingType,
      odometer: json.odometer,
      title: json.title,
      state: json.state,
      price: json.price,
      sellerType: json.sellerType,
      photos: json.photos,
      features: json.features.reduce((features, item) => {
        const current = features.find((x) => x.group === item.group);
        if (current) {
          current.name = current.name.concat(", ", item.name);
        } else {
          features.push(item);
        }
        return features;
      }, []),
    };

    return car;
  }

  private;
}
